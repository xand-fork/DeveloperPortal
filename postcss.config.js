module.exports = {
  plugins: [
    require('postcss-import'),
    require('postcss-mixins'),
    require('postcss-simple-vars')({ silent: true }),
    require('tailwindcss/nesting'),
    require(`tailwindcss`)(`./tailwind.config.js`),
    require('postcss-include'),
    require(`autoprefixer`),
    ...(process.env.NODE_ENV === "production"
      ? [
          require(`cssnano`)({
            preset: "default",
          }),
        ]
      : []),
  ],
};
