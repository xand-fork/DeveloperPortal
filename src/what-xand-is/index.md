---
parent: what-xand-is
layout: layouts/layout-page
description: What Xand Is Placeholder Page
date: 2022-07-15T22:51:41.345Z
navHeader: What Xand Is
title: What Xand Is
tags:
  - what-xand-is
  - pages
---

# What Xand Is

Xand is a private settlement network that allows for a fast, non-reversible, bank-agnostic, 24x7 payment solution system that operates within the existing U.S. legal and regulatory frameworks. Whereas transactions on traditional payment rails can take up to a week to settle, and can be reversed, Xand instead settles transactions in under 60 seconds, making your funds immediately available for use.

To use Xand, Members (the primary users of the Xand Network) convert their bank funds into digital dollars by transferring money from their bank account into the Network’s trust account at the same bank. Members can then use the Xand Network's streamlined Send and Redeem workflows to transmit digital dollars within the Xand network.

* **Create** moves funds  from a Member's bank deposit account into the Xand network, creating digital dollars on the blockchain.
* **Send** transfers funds in the Xand network to other Members.
* **Redeem** moves funds from the Xand network back into a Member's bank deposit account.

Transfers between network Members, as well as redemptions are also recorded as transactions on the blockchain. The immutable nature of the blockchain is what allows for the fast, final nature of Xand transactions. This is all supported by Validators, who secure the blockchain via proof-of-authority consensus method, without the high energy costs of a proof-of-work algorithm. Cryptographic assurances guarantee your transactions and safeguard the confidentiality of Members and of transactions within the Network.

The Xand Network has its own on-chain governance, which allows Members to decide on membership  and to change Validators and other service providers  as required. The result is a dynamic network that will continue to support the needs of its members over time.

# Whitepaper

* [Xand Technical White Paper](https://transparent.us/docs/xand-technical-white-paper.pdf) — Information on the technical aspects of the Xand Network.
* [Confidentiality for the Xand Blockchain](https://transparent.us/docs/xand-confidentiality-white-paper.pdf) — Cryptographic model behind Xand's confidentiality protections.
