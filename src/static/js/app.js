// import Prism from 'prismjs';

// import 'prismjs/components/prism-javascript';
// import 'prismjs/components/prism-css';
// import 'prismjs/components/prism-markup';
// import 'prismjs/components/prism-rust';
// import 'prismjs/components/prism-yaml';

// import 'prismjs/plugins/toolbar/prism-toolbar';
// import 'prismjs/plugins/copy-to-clipboard/prism-copy-to-clipboard';
// import 'prismjs/plugins/line-numbers/prism-line-numbers';

/* PrismJS 1.25.0
https://prismjs.com/download.html#themes=prism-tomorrow&languages=markup+css+clike+javascript+rust+yaml&plugins=line-numbers+autoloader+normalize-whitespace+toolbar+copy-to-clipboard */



Prism.hooks.add("before-highlight", function (env) {
    env.code = env.element.innerText;
});
// Prism.highlightElement();

// window.subnavState = true;

window.onload = function () {
	// alert('hello world!');
	let prismLights = document.querySelectorAll('.prism-light');
	if (prismLights) {
		prismLights.forEach(function(el) {
			el.closest('pre').classList.add('prism-light');
			el.closest('.code-toolbar').classList.add('prism-light');
		});
	}
}


