---
layout: layouts/layout-xandbox
classes: narrow
title: Xandbox
navHeader: Xandbox
hideChannel: true
tags:
  - xandbox
hero: 
  src: "/static/img/hero-xandbox.svg"
  url: "/xandbox#request-access"
introduction:
  header: "Fully Featured Network Private Release Page"
  body: "Xandbox is not a mockup or a specification. Even better, it’s always up-to-date with the latest shipped codebase: Xandbox is the first place to see Transparent’s newest work on Xand. When you’re ready to deploy, the provided API is identical to the production network API, which makes it easier to move your treasury management real-time prototypes into production."
video:
  header: Each Xandbox includes
  body: ""
  list: ['A fully specified chain', 'A set of five Validator nodes, which provide consensus for the chain updates', 'A set of two Member nodes, complete with Member APIs']
  image:
    src: "/static/img/video.png"
	url: "https://youtube.com"
---

##### Preview Instant Transactions

Your business or group of counterparties can now test out the Xand network in Xandbox, a sandbox of local testnets that allows you to thoroughly analyze how Xand works, model out setups and integrations, and test configurations before deploying into your production environment. The setup is easy, with full operating instructions for custom installations using docker-compose.
