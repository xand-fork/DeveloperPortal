---
layout: layouts/layout-page
title: Member API Overview
parent: api
navHeader: API
description: Member API Overview Page
date: 2022-07-15T22:51:41.345Z
tags:
  - api
  - member
  - pages
---

# Member API Introduction

The Xand network can be accessed by a Member API, a RESTful API that gives you as a Member the ability to transact within the Xand network. The Rest endpoint for Member API commands is <code class="language-* prism-light">/api/{version}/</code> on the machine and port where Xand network software is running. 

The standard REST architecture is used, with resources accessible using the <code class="language-* prism-light">GET</code>, <code class="language-* prism-light">POST</code>, <code class="language-* prism-light">PUT</code>, and <code class="language-* prism-light">DELETE</code> HTTP request methods. When additional data must be sent to the Xand network, it's done using the request body of a <code class="language-* prism-light">POST</code> or <code class="language-* prism-light">PUT</code> command. Each message also includes an <code class="language-* prism-light">Authorization:</code> header that contains a Bearer token authenticating the user's identity.

A full message for the simple <code class="language-* prism-light">/member/address</code> <code class="language-* prism-light">GET</code> command, accessed through <code class="language-* prism-light">curl</code>, is formatted as follows:

```bash{.language-yaml}
curl -X GET "http://localhost:3000/api/v1/member/address" -H  "accept: application/json" -H  "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJYQU5EX05FVFdPUktfR0VORVJBVE9SIiwic3ViIjoiREVWRUxPUEVSIiwiZXhwIjo0MTAyMzU4NDAwLCJjb21wYW55IjoiQVdFU09NRUNPIn0.wMe5Ji5CKhK5xcOP9RR0mIYUGoB8HVs6NZbN0PtKeIM"
```
The API endpoints for the Xand Member API are currently divided into five categories:

* [/accounts/](/api/accounts) - View participant accounts.
* [/banks/](/api/banks) - View participating banks.
* [/member/](/api/member) - View current user.
* [/service/](/api/service) -  Examine overall service.
* [/transactions/](/api/transactions) - Create, transfer, or redeem claims.

The heart of the system is in the transactions, which follow the standard Xand workflow: claims are created to fund Members on the Xand network, using bank deposit accounts; claims are transferred among Members on the Xand network to make payments; and claims are redeemed to return funds to bank deposit accounts from the Xand network.
