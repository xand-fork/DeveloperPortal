---
layout: layouts/layout-page
title: Member API Service Route
parent: member
navHeader: API
description: Member API Service Route
date: 2022-07-15T22:51:41.345Z
tags:
  - api
  - member
  - pages
---


## /service/

The service is the Xand network.

* <code class="language-* prism-light">GET /service/health</code>: Enumerate the status for the Xand Network.

### GET /service/health

**Purpose:** Assess the health of the Xand Network.

**Response:** Returns a JSON object contains <code class="language-* prism-light">up</code> and <code class="language-* prism-light">down</code> arrays, each of which lists the status of various Xand-network services.
```json
{
  "up": [
    {
      "service": "Xand API",
      "status": "up"
    },
    {
      "service": "Banks storage backend",
      "status": "up"
    },
    {
      "service": "Accounts storage backend",
      "status": "up"
    },
    {
      "service": "secret store",
      "status": "up"
    },
    {
      "service": "test-bank-one",
      "status": "up"
    },
    {
      "service": "test-bank-two",
      "status": "up"
    }
  ],
  "down": []
}
```
The above shows a fully functional Xand-network system, with all services functional.
