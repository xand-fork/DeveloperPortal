---
layout: layouts/layout-page
title: Member API Accounts Route
parent: member
navHeader: API
description: Member API Accounts Route
date: 2022-07-15T22:51:41.345Z
tags:
  - api
  - member
  - pages
---

## /accounts/

Accounts are the Xand-network representations of the Members' bank accounts.  Each account is associated with a specific <code class="language-* prism-light">bank</code> where the Member holds a deposit account; this is the deposit account used for claim creation and redemption.

* <code class="language-* prism-light">GET /accounts</code>: List all accounts.
* <code class="language-* prism-light">POST /accounts</code>: Register a new account.
* <code class="language-* prism-light">DELETE /accounts/{accountId}</code>: Remove an account's registration.
* <code class="language-* prism-light">GET /accounts/{accountId}</code>: Retrieve information for a specific account.
* <code class="language-* prism-light">PUT /accounts/{accountId}</code>: Edit an account's information.
* <code class="language-* prism-light">GET /accounts/{accountId}/balance</code>: Get the fiat balance of an account.

### GET /accounts 

**Purpose:** List all accounts registered for a Xand network.

**Response:** Returns a JSON array of objects, each of which contains information on an account registered on the Xand network.

_Example:_
```json
[
  {
    "id": 1,
    "bankId": 1,
    "shortName": "Account 0",
    "bankName": "test-bank-one",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "990000000"
  },
  {
    "id": 2,
    "bankId": 2,
    "shortName": "Account 0",
    "bankName": "test-bank-two",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "991000000"
  }
]
```
Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### POST /accounts <a name="post-accounts"></a>

**Purpose:** Register a new account on the Xand network.

**Request Body:** Requires a JSON object that names the account and links it appropriately to a bank deposit account.

_Example:_
```json
{
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "accountNumber": "1234567890"
}
```

* <code class="language-* prism-light">shortName</code>: The short, human-readable name for the account.
* <code class="language-* prism-light">bankId</code>: The identifier of the Member's deposit bank.
* <code class="language-* prism-light">accountNumber</code>: The number of the Member's deposit account at the bank.

**Response:** Returns a JSON object containing the full description of the account, exactly as would be returned by <code class="language-* prism-light">GET /accounts</code>.

_Example:_
```json
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful registration.
* <code class="language-* prism-light">400</code>: Input or other client error.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Bank account not found.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### DELETE /accounts/{accountId}

**Purpose:** Delete the Xand-network registration of an account.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful deletion.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.

### GET /accounts/{accountId}

**Purpose:** Retrieve information on a specific account.

**Response:** Returns a JSON object containing the full description of the account, exactly as would be returned by <code class="language-* prism-light">GET /accounts</code>.

_Example:_
```json
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### PUT /accounts/{accountId}

**Purpose:** Update the name and/or deposit account registration for an account.

**Request Body:** Requires a JSON object containing the <code class="language-* prism-light">shortName</code> and <code class="language-* prism-light">accountNumber</code> values. Both keys must be included or the command will fail.

_Example:_
```json
{
  "shortName": "Discretionary Funds",
  "accountNumber": "1234567890"
}
```
* <code class="language-* prism-light">shortName</code>: The short, human-readable name for the account.
* <code class="language-* prism-light">accountNumber</code>: The number of the Member's deposit account at the bank.

Note that <code class="language-* prism-light">bankId</code> may not be updated for an account.

**Response:** Returns a JSON object containing the full description of the updated account, exactly as would be returned by <code class="language-* prism-light">GET /accounts</code>.

_Example:_
```json
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful update.
* <code class="language-* prism-light">400</code>: Input or other client error.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### GET /accounts/{accountId}/balance

**Purpose:** Determine the balance of an account.

**Response:** Returns a JSON object listing the fiat monetary balance of a deposit account.

_Example:_
```json
{
  "accountId": "1",
  "currentBalanceInMinorUnit": 1000000000,
  "availableBalanceInMinorUnit": 1000000000
}
```

* <code class="language-* prism-light">accountId</code>: The account being checked.
* <code class="language-* prism-light">currentBalanceInMinorUnit</code>: Full balance in a minor unit such as cents (for USD).
* <code class="language-* prism-light">availableBalanceInMinorUnit</code>: Available balance in a minor unit such as cents (for USD).

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.
