---
layout: layouts/layout-page
title: Member API Transactions Route
parent: member
navHeader: API
description: Member API Transactions Route
date: 2022-07-15T22:51:41.345Z
tags:
  - api
  - member
  - pages
---

## /transactions/

Transactions are the heart of the Xand network, allowing for the creation (<code class="language-* prism-light">create</code>/<code class="language-* prism-light">fund</code>), transfer (<code class="language-* prism-light">send</code>), and redemption (<code class="language-* prism-light">redeem</code>) of claims.

* <code class="language-* prism-light">GET /transactions</code>: Look up a list of transactions for the active Member.
* <code class="language-* prism-light">GET /transactions/{transactionId}</code>: Look up a specific transaction.
* <code class="language-* prism-light">POST /transactions/claims/create</code>: Begin the process of creating new claims.
* <code class="language-* prism-light">POST /transactions/claims/redeem</code>: Redeem claims to an account.
* <code class="language-* prism-light">POST /transactions/claims/send</code>: Transfer claims to another member.
* <code class="language-* prism-light">POST /transactions/claims/{transactionId}/fund</code>: Fund a newly created claim from an account.

### GET /transactions

**Purpose:** Retrieve all of a Member's transactions of all sorts.

**Query Parameters:**

* <code class="language-* prism-light">pageSize</code>: Number of transactions to return per page [default: 10].
* <code class="language-* prism-light">pageNumber</code>: Page of transactions to return [default: 0].

_Example:_
```json
/api/v1/transactions?pageSize=10&pageNumber=0
```

**Response:** Returns a JSON object with a count of transactions and a JSON array of those transactions.  

_Example:_
```json
{
  "total": 1,
  "transactions": [
    {
      "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
      "operation": "creationRequest",
      "signerAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
      "amountInMinorUnit": 100000,
      "destinationAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
      "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
      "bankAccount": {
        "id": 1,
        "bankId": 1,
        "shortName": "Account 0",
        "bankName": "test-bank-one",
        "maskedAccountNumber": "XXXXXXXX0000",
        "routingNumber": "990000000"
      },
      "status": {
        "state": "pending"
      },
      "datetime": "2022-03-14T18:15:18Z"
    }
  ]
}
```
* <code class="language-* prism-light">total</code>: Number of transactions.
* <code class="language-* prism-light">transactionId</code>: Identifier for a transaction.
* <code class="language-* prism-light">operation</code>: Type of transaction, which is <code class="language-* prism-light">creationRequest</code>, <code class="language-* prism-light">payment`, or <code class="language-* prism-light">redeem</code>.
* <code class="language-* prism-light">signerAddress</code>: Public address of sender.
* <code class="language-* prism-light">destinationAddress</code>: Public address of recipient (not included for redemptions).
* <code class="language-* prism-light">amountInMinorUnit</code>: Amount of funds in transaction, where minor unit is cents for USD.
* <code class="language-* prism-light">nonce</code> - Correlation ID used by Member and Trustee to link transactions to activities at deposit accounts (not included for transfers).
* <code class="language-* prism-light">bankAccount</code> - Information on bank account for a creation or redemption request (not included for transfers).
* <code class="language-* prism-light">state</code> - Current status of transaction, which is <code class="language-* prism-light">cancelled</code>, <code class="language-* prism-light">confirmed</code>, <code class="language-* prism-light">invalid</code>, or <code class="language-* prism-light">pending</code>.
* <code class="language-* prism-light">datetime</code> - Time of transaction in Zulu time (GMT/UTC).

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.

### GET /transactions/{transactionId}

**Purpose:** Look up information on a specific transaction.

**Response:** Returns a JSON object containing one transaction, exactly as would be returned by <code class="language-* prism-light">GET /transactions</code>.

_Example:_
```json
{
  "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
  "operation": "creationRequest",
  "signerAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
  "amountInMinorUnit": 100000,
  "destinationAddress": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz",
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
  "bankAccount": {
    "id": 1,
    "bankId": 1,
    "shortName": "Account 0",
    "bankName": "test-bank-one",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "990000000"
  },
  "status": {
    "state": "pending"
  },
  "datetime": "2022-03-14T18:15:18Z"
}
```
* <code class="language-* prism-light">transactionId</code>: Identifier for a transaction.
* <code class="language-* prism-light">operation</code>: Type of transaction, <code class="language-* prism-light">creationRequest</code>, <code class="language-* prism-light">payment</code>, or <code class="language-* prism-light">redeem</code>.
* <code class="language-* prism-light">signerAddress</code>: Public address of sender.
* <code class="language-* prism-light">destinationAddress</code>: Public address of recipient (not included for redemptions).
* <code class="language-* prism-light">amountInMinorUnit</code>: Amount of funds in transaction, where minor unit is cents for USD.
* <code class="language-* prism-light">nonce</code> - Correlation ID used by Member and Trustee to link transactions to activities at deposit accounts (not included for transfers).
* <code class="language-* prism-light">bankAccount</code> - Information on bank account for a creation or redemption request (not included for transfers).
* <code class="language-* prism-light">state</code> - Current status of transaction, which is <code class="language-* prism-light">cancelled</code>, <code class="language-* prism-light">confirmed</code>, <code class="language-* prism-light">invalid</code>, or <code class="language-* prism-light">pending</code>.
* <code class="language-* prism-light">datetime</code> - Time of transaction in Zulu time (GMT/UTC).


**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Transaction ID not found.
* <code class="language-* prism-light">500</code>: Server error, including incorrectly formatted Transaction ID.

### POST /transactions/claims/create <a name="transactions-claims-create"></a>

**Purpose:** Begin the process of claim creation for a Member.

**Request Body:** Requires a JSON object containing the <code class="language-* prism-light">accountId</code> that will fund the Member and the amount of funds.

_Example:_
```json
{
  "accountId": 1,
  "amountInMinorUnit": 100000
}
```

* <code class="language-* prism-light">accountId</code>: Identifier for the account providing funds.
* <code class="language-* prism-light">amountInMinorUnit</code> :- Amount of funds being send, where minor unit is cents for USD.

**Response:** Returns a JSON object containing the identifiers for the funding transaction.

_Example:_
```json
{
  "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0"
}
```
* <code class="language-* prism-light">transactionId</code>: Identifier for this transaction.
* <code class="language-* prism-light">nonce</code>: Correlation ID used by Member and Trustee to link transactions to activities at deposit accounts.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful creation.
* <code class="language-* prism-light">400</code>: Input or other client error.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error including Bank ID not found.

> **Cryptographic Notes:** The identity of the Member creating claims, their ABA routing number, and their bank account number are all masked to everyone but the Trustee and the Member. 

> **Financial Notes:** This request creates a record for a new claim and provides the Member with a <code class="language-* prism-light">nonce</code> to correlate the claim to a bank transfer. It is just the first part of a three-part process to transform fiat currency into claims on the Xand network:
> 
> * Create claims with <code class="language-* prism-light">POST /transactions/claims/create</code>.
> * Fund claims with <code class="language-* prism-light">POST /transactions/claims/{transactionId}/fund</code>.
> * Wait for Trustee to confirm funding.
>
> The steps are all connected by inclusion of the same `nonce` as a correlation ID.

### POST /transactions/claims/redeem <a name="transactions-claims-redeem"></a>

**Purpose:** Transfer claims back into fiat currency at the Member's bank account.

**Request Body:** Requires a JSON object containing the account that funds are being sent to and the amount of money being redeemed.

_Example:_
```json
{
  "accountId": 1,
  "amountInMinorUnit": 9999
}
```
* <code class="language-* prism-light">accountId</code>: The account receiving the redemption. This must be the account associated with the active Member, else funds will become inaccessible.
* <code class="language-* prism-light">amountInMinorUnit</code>: The amount of the redemption, where minor unit is cents for USD.

**Response:** Returns a JSON object containing a transaction ID and a nonce.
```json
{
  "transactionId": "0x1749f72ce95e18c9eae8c28c0a35aae9915effa2168f3bdb3e5312a368bf4ed4",
  "nonce": "0xe2e03b07c98c9489855bd63d5de5a773"
}
```
The <code class="language-* prism-light">nonce</code> may be used to correlate the transfer of funds into the Member's deposit account.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful creation.
* <code class="language-* prism-light">400</code>: Input or other client error, including Account ID not found.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error including Bank ID not found.

> **Cryptographic Notes:** The identity of the Member redeeming claims, their ABA routing number, and their bank account number are all masked to everyone but the Trustee and the Member. 

### POST /transactions/claims/send <a name="transactions-claims-send"></a>

**Purpose:** Sends claims from one Member to another.

**Request Body:** Requires a JSON object containing the recipient's address and the amount to transfer.

_Example:_
```json
{
  "toAddress": "5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj",
  "amountInMinorUnit": 233
}
```
* <code class="language-* prism-light">toAddress</code>: The public address of the recipient, retrieved by them using <code class="language-* prism-light">GET /member/address</code>.
* <code class="language-* prism-light">amountInMinorUnit</code>: Amount of funds being send, where minor unit is cents for USD.

**Response:** Returns a JSON object contain the transaction identifier and a nonce.

_Example:_
```json
{
  "transactionId": "0x457f6de36df3928ab192539f501762d84e4e6995cb10162d89df5c5629e029af",
  "nonce": null
}
```
Note that the <code class="language-* prism-light">nonce</code> is currently empty because there is no need to correlate a <code class="language-* prism-light">claims/send</code> transaction to a bank transaction.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful transfer.
* <code class="language-* prism-light">400</code>: Input or other client error.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** The sending Member's identity, the receiving Member's identity, and the transaction amount are all hidden from everyone but the Members involved in the transaction.

> **Financial Notes:** This is the main function of the Xand network. It's how Members transfer funds (Claims) to each other, enabling real-time payment.

### POST /transactions/claims/{transactionId}/fund <a name="transactions-claims-fund"></a>

**Purpose:** Fund a claim that has previously been created by transferring money from the Member's deposit account to the Trustee's deposit account at a bank.

**Request Body:** Requires a JSON object containing the transferring amount, the nonce for the transaction, and the bank being used.

_Example:_
```json
{
  "amountInMinorUnit": 100000,
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
  "bankAccountId": 1
}
```
* <code class="language-* prism-light">amountInMinorUnit</code>: Amount of funds being sent, where minor unit is cents for USD.
* <code class="language-* prism-light">none</code>: Correlation ID from `/create` command.
* <code class="language-* prism-light">bankAccountId</code>: Identifier for the Member's account.

The <code class="language-* prism-light">nonce</code> and <code class="language-* prism-light">amountInMinorUnit</code> both must match the claims creation transaction and the <code class="language-* prism-light">bankAccountId</code> must match the <code class="language-* prism-light">accountId</code> associated with the member. Incorrectly inputting the transaction information may transfer funds to the Trustee but will not close the claim creation. Incorrectly inputting the <code class="language-* prism-light">bankAccountId</code> will result in nothing occurring.

**Response:** Returns a JSON object reiterating the amount transferred.

_Example:_
```json
{
  "amountInMinorUnit": 100000
}
```

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful funding.
* <code class="language-* prism-light">400</code>: Input or other client error.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** The identity of the Member creating claims, their ABA routing number, and their bank account number are all masked to everyone but the Trustee and the Member. 

> **Financial Notes:** This request transfers funds from the Member's deposit account to the Trustee's deposit account at the same bank. It is the second part of a three-part process to transform fiat currency into claims on the Xand network:
> 
> * Create claims with <code class="language-* prism-light">POST /transactions/claims/create</code>.
> * Fund claims with <code class="language-* prism-light">POST /transactions/claims/{transactionId}/fund</code>.
> * Wait for Trustee to confirm funding.


Accounts are the Xand-network representations of the Members' bank accounts.  Each account is associated with a specific bank where the Member holds a deposit account; this is the deposit account used for claim creation and redemption.

* <code class="language-* prism-light">GET /accounts</code>: List all accounts.
* <code class="language-* prism-light">POST /accounts</code>: Register a new account.
* <code class="language-* prism-light">DELETE /accounts/{accountId}</code>: Remove an account's registration.
* <code class="language-* prism-light">GET /accounts/{accountId}</code>: Retrieve information for a specific account.
* <code class="language-* prism-light">PUT /accounts/{accountId}</code>: Edit an account's information.
* <code class="language-* prism-light">GET /accounts/{accountId}/balance</code>: Get the fiat balance of an account.

### GET /accounts

**Purpose:** List all accounts registered for a Xand network.

**Response:** Returns a JSON array of objects, each of which contains information on an account registered on the Xand network.

_Example:_
```json
[
  {
    "id": 1,
    "bankId": 1,
    "shortName": "Account 0",
    "bankName": "test-bank-one",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "990000000"
  },
  {
    "id": 2,
    "bankId": 2,
    "shortName": "Account 0",
    "bankName": "test-bank-two",
    "maskedAccountNumber": "XXXXXXXX0000",
    "routingNumber": "991000000"
  }
]
```
Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### POST /accounts

**Purpose:** Register a new account on the Xand network.

**Request Body:** Requires a JSON object that names the account and links it appropriately to a bank deposit account.

_Example:_
```json
{
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "accountNumber": "1234567890"
}
```

* <code class="language-* prism-light">shortName</code>: The short, human-readable name for the account.
* <code class="language-* prism-light">bankId</code>: The identifier of the Member's deposit bank.
* <code class="language-* prism-light">accountNumber</code>: The number of the Member's deposit account at the bank.

**Response:** Returns a JSON object containing the full description of the account, exactly as would be returned by <code class="language-* prism-light">GET /accounts</code>.

_Example:_
```json
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful registration.
* <code class="language-* prism-light">400</code>: Input or other client error.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Bank account not found.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### DELETE /accounts/{accountId}

**Purpose:** Delete the Xand-network registration of an account.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful deletion.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.

### GET /accounts/{accountId}

**Purpose:** Retrieve information on a specific account.

**Response:** Returns a JSON object containing the full description of the account, exactly as would be returned by <code class="language-* prism-light">GET /accounts</code>.

_Example:_
```json
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this Member.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### PUT /accounts/{accountId}

**Purpose:** Update the name and/or deposit account registration for an account.

**Request Body:** Requires a JSON object containing the <code class="language-* prism-light">shortName</code> and <code class="language-* prism-light">accountNumber</code> values. Both keys must be included or the command will fail.

_Example:_
```json
{
  "shortName": "Discretionary Funds",
  "accountNumber": "1234567890"
}
```
* <code class="language-* prism-light">shortName</code>: The short, human-readable name for the account.
* <code class="language-* prism-light">accountNumber</code>: The number of the Member's deposit account at the bank.

Note that <code class="language-* prism-light">bankId</code> may not be updated for an account.

**Response:** Returns a JSON object containing the full description of the updated account, exactly as would be returned by <code class="language-* prism-light">GET /accounts</code>.

_Example:_
```json
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

Each object contains the following information about the account:

* <code class="language-* prism-light">id</code>: The identifier for the account.
* <code class="language-* prism-light">shortName</code>: The short, human-readable name of the account.
* <code class="language-* prism-light">maskedAccountNumber</code>: The deposit account at the bank associated with this account.

Each object also contains the following information about the bank associated with the account:

* <code class="language-* prism-light">bankId</code>: The identifier of the bank.
* <code class="language-* prism-light">bankName</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful update.
* <code class="language-* prism-light">400</code>: Input or other client error.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.

> **Cryptographic Notes:** Deposit account numbers are masked to maintain confidentiality.

### GET /accounts/{accountId}/balance

**Purpose:** Determine the balance of an account.

**Response:** Returns a JSON object listing the fiat monetary balance of a deposit account.

_Example:_
```json
{
  "accountId": "1",
  "currentBalanceInMinorUnit": 1000000000,
  "availableBalanceInMinorUnit": 1000000000
}
```

* <code class="language-* prism-light">accountId</code>: The account being checked.
* <code class="language-* prism-light">currentBalanceInMinorUnit</code>: Full balance in a minor unit such as cents (for USD).
* <code class="language-* prism-light">availableBalanceInMinorUnit</code>: Available balance in a minor unit such as cents (for USD).

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Account ID not found.
* <code class="language-* prism-light">500</code>: Server error.
