---
layout: layouts/layout-page
title: Member API Member Route
parent: member
navHeader: API
description: Member API Member Route
date: 2022-07-15T22:51:41.345Z
tags:
  - api
  - member
  - pages
---

## /member/

A Member is the representation of a person or entity on the Xand network. They are identified by their <code class="language-* prism-light">address</code>. They have an <code class="language-* prism-light">account</code>, which is associated with a <code class="language-* prism-light">bank</code> and holds fiat currency. They also hold claims on the Xand network, which may be funded from their <code class="language-* prism-light">account</code>, which may be sent to other Members, and which may be redeemed to their <code class="language-* prism-light">account</code>.  Member API commands refer to the currently authenticated user on the Xand network.

* <code class="language-* prism-light">GET /member/address</code>: Looks up the authenticated user's public address.
* <code class="language-* prism-light">GET /member/balance</code>: Looks up the authenticated user's claims balance.

### GET /member/address

**Purpose:** Reveal the public identifier for the active Member's account, which is used to send them money.

**Response:** Returns a JSON object containing an <code class="language-* prism-light">address</code>.
```json
{
  "address": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz"
}
```

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.

### GET /member/balance

**Purpose:** Retrieve the balance for the active Member's account.

**Response:** Returns a JSON object with one key-pair, the balance in minor units (cents for USD).

_Example:_
```json
{
  "balanceInMinorUnit": 0
}
```
* <code class="language-* prism-light">balanceInMinorUnit</code>: Claims balance in a minor unit such as cents (for USD).

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.
