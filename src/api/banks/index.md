---
layout: layouts/layout-page
title: Member API Banks Route
parent: member
navHeader: API
description: Member API Banks Route
date: 2022-07-15T22:51:41.345Z
tags:
  - api
  - member
  - pages
---

## /banks/

Participating banks give members the ability to deposit money in order to create claims on the Xand network and to receive money when they redeem claims on the Xand network. The Trustee has an account at each participating bank for a network, for receiving and distributing funds for the network. A Member will have a deposit account at one of the participating banks, which will be recorded as their <code class="language-* prism-light">account</code>. They will interact with the Trustee deposit account at the same bank when creating and redeeming claims.

* <code class="language-* prism-light">GET /banks</code>: List all banks.
* <code class="language-* prism-light">POST /banks</code>: Register a new bank.
* <code class="language-* prism-light">DELETE /banks/{bankId}</code>: Remove a bank's registration.
* <code class="language-* prism-light">GET /banks/{bankId}</code>: Retrieve information for a specific bank.
* <code class="language-* prism-light">POST /banks/{bankId}</code>: Edit a bank's information.

### GET /banks/ <a name="get-banks"></a>

**Purpose:** Retrieve a list of banks registered with a Xand network.

**Response:** Returns a JSON array of JSON objects, each describing a bank.

_Example:_
```json
[
  {
    "id": 1,
    "name": "test-bank-one",
    "routingNumber": "<Routing Number>",
    "trustAccount": "<Trust Account Number>",
    "adapter": {
      "<Bank Adapter Name>": {
        "url": "<Bank API URL>",
        "secretApiKeyId": "<API Key Id Secret Key>",
        "secretApiKeyValue": "<API Key Value Secret Key>"
      }
    }
  },
  {
    "id": 2,
    "name": "test-bank-two",
    "routingNumber": "<Routing Number>",
    "trustAccount": "<Trust Account Number>",
    "adapter": {
      "<Bank Adapter Name>": {
        "url": "<Bank API URL>",
        "secretApiKeyId": "<API Key Id Secret Key>",
        "secretApiKeyValue": "<API Key Value Secret Key>"
      }
    }
  }
]
```
Each bank lists the following information:

* <code class="language-* prism-light">id</code>: The identifier of the bank.
* <code class="language-* prism-light">name</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.
* <code class="language-* prism-light">trustAccount</code>: The deposit account for the Trustee at the bank.
* <code class="language-* prism-light">adapter</code>: Information for accessing the bank's API.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">500</code>: Server error.

> **Financial Notes:** When claims are funded, money is transferred from a Member's deposit account to the Trustee account associated with the Member's bank; when claims are redeemed, money is transferred from the Trustee account to the Member's deposit account.

### POST /banks <a name="post-banks"></a>

**Purpose:** Register a bank with a Xand network.

**Request Body:** Requires a JSON object containing the full information on a bank as would be retrieved by <code class="language-* prism-light">GET /banks</code>, except without the <code class="language-* prism-light">id</code> (which will be returned) and with only one <code class="language-* prism-light">adapter</code>.

_Example:_
```json
{
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```
* <code class="language-* prism-light">name</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.
* <code class="language-* prism-light">trustAccount</code>: The deposit account for the Trustee at the bank.
* <code class="language-* prism-light">adapter</code>: Information for accessing the bank's API. For a `"<Bank Name>"` adapter, include: <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretApiKeyId</code>, and <code class="language-* prism-light">secretApiKeyValue</code>. For a `"<Bank Name>"` adapter, include <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretUserName</code>, <code class="language-* prism-light">secretPassword</code>, <code class="language-* prism-light">secretClientAppIdent</code>, and <code class="language-* prism-light">secretOrganizationId</code>.

**Response:** Returns a JSON object containing the input bank, plus an <code class="language-* prism-light">id</code>, exactly as would be returned by <code class="language-* prism-light">GET /banks</code>.

_Example:_
```json
{
  "id": <Bank Id>,
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful registration.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Bank ID not found.
* <code class="language-* prism-light">500</code>: Server error.

### DELETE /banks/{bankId}

**Purpose:** Remove a bank's registration from a Xand network.

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful deletion.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Bank ID not found.
* <code class="language-* prism-light">500</code>: Server error.

### GET /banks/{bankId}

**Purpose:** Retrieve information on a specific bank.

**Response:** Displays information on a specific bank, exactly as would be returned by <code class="language-* prism-light">GET /banks</code>.
```json
{
  "id": <Bank Id>,
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```
* <code class="language-* prism-light">id</code>: The identifier of the bank.
* <code class="language-* prism-light">name</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.
* <code class="language-* prism-light">trustAccount</code>: The deposit account for the Trustee at the bank.
* <code class="language-* prism-light">adapter</code>: Information for accessing the bank's API. 

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful retrieval.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Bank ID not found.
* <code class="language-* prism-light">500</code>: Server error.

### PUT /banks/{bankId}

**Purpose:** Update information on a specific bank.

**Request Body:** Requires a JSON object containing the full information on a bank as would be entered through <code class="language-* prism-light">POST /banks</code>. Any data may be updated other than the ID.

_Example:_
```json
{
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```
* <code class="language-* prism-light">name</code>: The human-readable name of the bank.
* <code class="language-* prism-light">routingNumber</code>: The ABA routing number for the bank.
* <code class="language-* prism-light">trustAccount</code>: The deposit account for the Trustee at the bank.
* <code class="language-* prism-light">adapter</code>: Information for accessing the bank's API. For a `"<Bank Name>"` adapter, include: <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretApiKeyId</code>, and <code class="language-* prism-light">secretApiKeyValue</code>. For a `"<Bank Name>"` adapter, include <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretUserName</code>, <code class="language-* prism-light">secretPassword</code>, <code class="language-* prism-light">secretClientAppIdent</code>, and <code class="language-* prism-light">secretOrganizationId</code>.


**Response:** Returns a JSON object containing the updated bank, plus an <code class="language-* prism-light">id</code>, exactly as would be returned by <code class="language-* prism-light">GET /banks</code>.

_Example:_
```json
{
  "id": <Bank Id>,
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API Url>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```

**Response Codes:**

* <code class="language-* prism-light">200</code>: Successful update.
* <code class="language-* prism-light">401</code>: Authentication error.
* <code class="language-* prism-light">404</code>: Bank ID not found.
* <code class="language-* prism-light">500</code>: Server error.
