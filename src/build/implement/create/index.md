---
layout: layouts/layout-page
title: Create On Xand Network
parent: implement
navHeader: Create
description: Create On Xand Network Placeholder Page
tags:
  - build
  - implement
  - pages
---
# Implement: Create

Once you have configured your bank information and retrieved your Member address, you can create claims on the Xand network, which is what makes money in your bank deposit account available for sending on the network. The create process works by transferring money from your bank deposit account to a Trustee account at your bank (also known as "funding" your create request). Once that occurs, a corresponding claim is then created on the Xand network, which allows you to transfer up to that value to other Members.

This is a two-step process:

* <code class="language-* prism-light">POST /transactions/claims/create</code> - Initiate an creation claim for a Member.
* <code class="language-* prism-light">POST /transactions/claims/{transactionId}/fund</code> - Fund an existing creation claim.

The Claim workflow may be used whenever you want to add funds to your Member account.

## POST /transactions/claims/create
#### Initiate a creation claim for a Member.

[ [API Documentation](/api/transactions#transactions-claims-create)&nbsp;]

To begin the process of creating claims on the Xand Network, use <code class="language-* prism-light">POST /transactions/claims/create</code>, which will initiate a claim-creation request that will be valid for 24 hours. You must supply a bank <code class="language-* prism-light">accountId</code> that you retrieved during the Configuration step, and an amount. You will receive a response with a <code class="language-* prism-light">transactionId</code> and a <code class="language-* prism-light">nonce</code>, which you use to fund your claim in the next step, **required to complete your claim creation**.
>**Note:** The amount will be transferred from your bank deposit account to the Xand Network in the funding step

### Example Request

This indicates an eventual creation of 100,000 units on the Member's ledger on the Xand network, which is $1,000.00 if the unit is USD, from bank <code class="language-* prism-light">accountID</code> <code class="language-* prism-light">1</code> in the system.

```json
{
  "accountId": 1,
  "amountInMinorUnit": 100000
}
```

### Request Fields

#### accountId
[required, integer]

Identifier for the bank deposit account providing funds, can be retrieved from <code class="language-* prism-light">POST /accounts</code>

#### amountInMinorUnit
[required, integer]

Amount of funds that will be transferred into a claim, where minor unit is cents for USD.

### Example Response
```json
{
  "transactionId": "0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8",
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0"
}
```

### Response Fields

#### transactionId
[string (hex)]

Identifier for this transaction. To be used in the creation funding step. 

#### nonce
[string]

Correlation ID. To be used in the creation funding step.

## POST /transactions/claims/{transactionId}/fund
#### Fund an existing creation claim.

[ [API Documentation](/api/transactions#transactions-claims-fund)&nbsp;]

To complete the process of creating claims on the Xand network, use <code class="language-* prism-light">POST /transactions/claims/{transactionId}/fund</code>, where <code class="language-* prism-light">{transactionId}</code> was retrieved in the response body from the <code class="language-* prism-light">POST /transactions/claims/create</code> call that initiated this process. This will transfer the funds from your bank deposit account to the Trustee account, and after the Trustee has verified the transfer, will then create a corresponding claim on the Xand network. You may then Send claims to other members and also may Redeem claims back into your bank account as desired.

### Example Request

This API call funds the $1,000 USD claim creation initiated above, and in this case is sent as a request to the endpoint <code class="language-* prism-light break-line">/transactions/claims/0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8/fund</code>, where the <code class="language-* prism-light">transactionId</code> is <code class="language-* prism-light break-line">0xa138ea5b6f990b11906479b22e1b077bfbd35453be1b6106c1ffc0e8cdd812e8</code>.

```json
{
  "amountInMinorUnit": 100000,
  "nonce": "0x9169995b8c132c96e7e94c337fc01bf0",
  "bankAccountId": 1
}
```

### Request Fields

#### amountInMinorUnit
[required, integer]

Amount of funds being utilized to fund the create claim, where minor unit is cents for USD. This must match the <code class="language-* prism-light">amountInMinorUnit</code> sent with the <code class="language-* prism-light">POST /transactions/claims/create</code> request.

#### nonce
[required, string]

Correlation ID. This **must** match the <code class="language-* prism-light">nonce</code> retrieved from the <code class="language-* prism-light">POST /transactions/claims/create</code> response. Incorrect entry of the <code class="language-* prism-light">nonce</code> may result in funds being transferred out of your bank deposit account without claims being created.

#### bankAccountId
[required, integer]

Identifier for the Member's registered bank account. This must match the <code class="language-* prism-light">accountId</code> sent with the <code class="language-* prism-light">POST /transactions/claims/create</code> request. Incorrect entry of the <code class="language-* prism-light">bankAccountId</code> will likely result in nothing occurring.

### Example Response
```json
{
  "amountInMinorUnit": 100000
}
```

### Response Fields

### amountInMinorUnit
[required, integer]

Amount of funds being used to fund the create claim, where minor unit is cents for USD.

