---
title: Implement
description: Implement Page
date: 2022-07-18T22:11:02.168Z
anchorlinks:
  - title: Create Xand
    description: Fund your balance on the Xand network from your bank account
    icon: /src/_includes/svgs/icon-tools.svg
    url: /build/implement/create
  - title: Send Xand
    description: Transact on the Xand network with other participants
    icon: /src/_includes/svgs/icon-tools.svg
    url: /build/implement/send
  - title: Redeem Xand
    description: Redeem funds from your Xand network balance to your bank account
    icon: /src/_includes/svgs/icon-tools.svg
    url: /build/implement/redeem
tags:
  - pages
  - build
---
# Implement

Xand allows for the rapid </a>[trustless transfer](/what-xand-is) of funds between members using the following general workflow:

* **Configure.** Bank deposit accounts are registered with the Xand network.
* **Create.** Claims are created to fund Members on the Xand network, using bank deposit accounts.
* **Send.** Claims are transferred among Members on the Xand network to make payments.
* **Redeem.** Claims are redeemed to return funds to bank deposit accounts from the Xand network.

Xand is accessed through a RESTful API. The Rest endpoint for Member API commands is <code class="language-* prism-light">/api/{version}/</code> on the machine and port where Xand is running.

The standard REST architecture is used, with resources accessible using the GET, POST, PUT, and DELETE HTTP request methods. When additional data must be sent to Xand, it's done using the request body of a POST or PUT command.

A full message for the simple <code class="language-* prism-light">/member/address</code> GET command, accessed through curl, would be formatted as follows:

```bash
curl -X GET "http://localhost:3000/api/v1/member/address" -H  "accept: application/json" -H  "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJYQU5EX05FVFdPUktfR0VORVJBVE9SIiwic3ViIjoiREVWRUxPUEVSIiwiZXhwIjo0MTAyMzU4NDAwLCJjb21wYW55IjoiQVdFU09NRUNPIn0.wMe5Ji5CKhK5xcOP9RR0mIYUGoB8HVs6NZbN0PtKeIM"
```

The API endpoints for the Xand Member API are currently divided into five categories:

* [/accounts/](/api/accounts) - View participant accounts.
* [/banks/](/api/banks) - View participating banks.
* [/member/](/api/member) - View current user.
* [/service/](/api/service) -  Examine overall service.
* [/transactions/](/api/transactions) - Create, transfer, or redeem claims.

