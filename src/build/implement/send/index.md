---
layout: layouts/layout-page
title: Send On Xand Network
parent: implement
navHeader: Send
description: Send On Xand Network Placeholder Page
tags:
  - build
  - implement
  - pages
---

# Implement: Send

You can receive claims on your Member address, either because you followed the Create workflow or because other Members sent you claims. Once you have claims, you can send them to other Members of the Xand Network. This will allow you to make payments to those Members using the fast, final transactions of the Xand Network. All that is required to initiate a transaction is the address of the Member that you're sending to. 

This is a one-step process:

* <code class="language-* prism-light">POST /transactions/claims/send</code> — Sends claims from one Member to another.

> **Cryptographic Protections:** When using the system, all transaction information, including the identity of the sender, the identity of the recipient, and the transaction amount, is masked, except to the sender, the recipient, and the Trustee.

## POST /transactions/claims/send
#### Sends claims from one Member to another.

[ [API Documentation](/api/transactions#transactions-claims-send)&nbsp;]

To send a transaction to another Member, first you must know their address. Communication of this address should usually be managed in some secure out-of-band way, such as the other Member sending it over a secure-messaging system, and then verifying over voice or video. Once you have a destination address, you can use the <code class="language-* prism-light">POST /transactions/claims/send</code> command to send a chosen amount of claims to the address. 

After a transaction has been sent, settlement will be complete in less than 60 seconds. You may see the state of the transaction with the <code class="language-* prism-light">GET /transactions</code> or <code class="language-* prism-light">GET /transactions/{transactionid}</code> commands and after its <code class="language-* prism-light">state</code> has moved from <code class="language-* prism-light">pending</code> to <code class="language-* prism-light">confirmed</code>, the recipient will see it added to their balance with the <code class="language-* prism-light">GET /member/balance</code> command.

### Example Request

This sends 233 units, which is $2.33 if the unit is USD, to the Member identified by the address <code class="language-* prism-light break-line">5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj</code>.

```json
{
  "toAddress": "5GedXksPiCaXqv114r2NPQQbgirgA9X8pyRM1AHZ42bDGomj",
  "amountInMinorUnit": 233
}
```

### Request Fields

#### toAddress
[required, string]

The public address of the recipient.
> **Note**: See <code class="language-* prism-light">GET /member/address</code> for own address retrieval instructions. 

#### amountInMinorUnit
[required, int]

Amount of funds being sent, where minor unit is cents for USD.

### Example Response

```json
{
  "transactionId": "0x457f6de36df3928ab192539f501762d84e4e6995cb10162d89df5c5629e029af",
  "nonce": null
}
```

### Response Fields

#### transactionId
[string]

The identifier for the transaction, which can be referenced with the <code class="language-* prism-light">GET /transactions</code> or <code class="language-* prism-light">GET /transactions/{transactionid}</code> commands.

#### nonce
[string]

Empty Field.

> **Note:** <code class="language-* prism-light">nonce</code> is a correlation ID used as part of the Create or Redeem workflows. It will be empty when you utilize the Send workflow described here.
