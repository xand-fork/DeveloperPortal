---
layout: layouts/layout-page
title: Configure A Xand Network
parent: implement
navHeader: Configure
description: Configure A Xand Network Placeholder Page
date: 2022-07-19T21:06:24.645Z
tags:
  - build
  - implement
  - pages
---
# Implement: Configure

Configuration of the Member API is necessary for a Member to transact against the Xand Network. It requires: 

* Discovering the identifier for the Member's bank or setting that identifier;
* Registering the Member's account with a link to that bank; and
* Retrieving an address that the Member can use to receive funds. 

At this point, you may follow the Create workflow to receive funds, or you may wait for other Members to send you funds, and then either resend them with the Send workflow or redeem them to your bank account with the Redeem workflow.

The Configure workflow is as follows:

* Authorize — Authenticates your machine to make calls to the Member API.
* <code class="language-* prism-light">GET /banks</code> - Checks if your bank is already registered.
* <code class="language-* prism-light">POST /banks</code> (optional) — Registers a bank & trustee account.
* <code class="language-* prism-light">POST /accounts</code> - Creates your Member account linked to that bank.
* <code class="language-* prism-light">GET /member/address</code> - Retrieves your Member address for receiving claims.

> **Cryptographic Protections:** When using the Xand Network, all banking information, including ABA routing numbers and deposit account numbers, will be masked to everyone except the Member owning the bank account, and the Trustee. 

## Authorize
#### Authenticates your machine to make calls to the Member API

You should already have a Bearer token that authorizes your identity on the Xand Network. Please contact your Xand Network administrator if you do not have this token, or file a [support ticket](mailto:support@transparent.us?subject=MemberAPI%20Authorization%20Support). You must include the Bearer token in each message that you send to the Member API. This is done with an <code class="language-* prism-light">Authorization</code> header, as seen in this <code class="language-* prism-light">curl</code> example of initiating a request to the Member API:

```bash
curl -X GET "http://localhost:3000/api/v1/member/address" -H  "accept: application/json" -H  "Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJYQU5EX05FVFdPUktfR0VORVJBVE9SIiwic3ViIjoiREVWRUxPUEVSIiwiZXhwIjo0MTAyMzU4NDAwLCJjb21wYW55IjoiQVdFU09NRUNPIn0.wMe5Ji5CKhK5xcOP9RR0mIYUGoB8HVs6NZbN0PtKeIM"
```

In this example <code class="language-* prism-light break-line">eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJYQU5EX05FVFdPUktfR0VORVJBVE9SIiwic3ViIjoiREVWRUxPUEVSIiwiZXhwIjo0MTAyMzU4NDAwLCJjb21wYW55IjoiQVdFU09NRUNPIn0.wMe5Ji5CKhK5xcOP9RR0mIYUGoB8HVs6NZbN0PtKeIM</code> is the body of the Bearer token credential. The complete header for use in Member API requests is:

```bash
"Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpc3MiOiJYQU5EX05FVFdPUktfR0VORVJBVE9SIiwic3ViIjoiREVWRUxPUEVSIiwiZXhwIjo0MTAyMzU4NDAwLCJjb21wYW55IjoiQVdFU09NRUNPIn0.wMe5Ji5CKhK5xcOP9RR0mIYUGoB8HVs6NZbN0PtKeIM"
```

If you are using the Swagger Member API interface for making calls to the API, you will instead authorize your API calls by clicking on the "Authorize" button at top right and entering your Bearer token.

## GET /banks
#### Checks if your bank is already registered

[ [API Documentation](/api/banks#get-banks)&nbsp;]

Usually, your participating bank will already be registered with the Xand Network. The <code class="language-* prism-light">GET /banks</code> command may be used to look up your bank's identifier in the system. When you use it, you will retrieve a list of banks. Locate the ABA routing number of your bank, record its <code class="language-* prism-light">id</code> to use as your <code class="language-* prism-light">bankId</code> and proceed to the <code class="language-* prism-light">POST /accounts</code> command to register your bank account at this bank. 

If you do not see your bank's ABA number, you must first register your bank using the <code class="language-* prism-light">POST /banks</code> command; this should usually not be the case.

### Example Request

No request body is used when accessing the <code class="language-* prism-light">GET /banks</code> endpoint.

### Example Response

```json
[
  {
    "id": 1,
    "name": "test-bank-one",
    "routingNumber": "<Routing Number>",
    "trustAccount": "<Trust Account Number>",
    "adapter": {
      "<Bank Adapter Name>": {
        "url": "<Bank API URL>",
        "secretApiKeyId": "<API Key Id Secret Key>",
        "secretApiKeyValue": "<API Key Value Secret Key>"
      }
    }
  },
  {
    "id": 2,
    "name": "test-bank-two",
    "routingNumber": "<Routing Number>",
    "trustAccount": "<Trust Account Number>",
    "adapter": {
      "<Bank Adapter Name>": {
        "url": "<Bank API URL>",
        "secretApiKeyId": "<API Key Id Secret Key>",
        "secretApiKeyValue": "<API Key Value Secret Key>"
      }
    }
  }
]
```

### Response Fields

#### id
[integer]

The identifier of the bank.

#### name
[string]

The human-readable name of the bank.

#### routingNumber
[integer]

The ABA routing number for the bank.

#### trustAccount
[integer]

The deposit bank account number for the Trustee at the bank.

#### adapter
[JSON object]

Defines the adapter used to communicate with a given bank's API. 

This will be one of the following: 

**`"<Bank Adapter Name>"`**
 `"<Bank Adapter Name>"` JSON object containing <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretApiKeyId</code>, and <code class="language-* prism-light">secretApiKeyValue</code> (for a(n) `"<Bank Name>"` bank)

**`"<Bank Adapter Name>"`**
`"<Bank Adapter Name>"` JSON object containing <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretUserName</code>, <code class="language-* prism-light">secretPassword</code>, <code class="language-* prism-light">secretClientAppIdent</code>, and <code class="language-* prism-light">secretOrganizationId</code> (for a(n) `"<Bank Name>"` bank)

## POST /banks
#### Registers a bank & trustee account

[ [API Documentation](/api/banks#post-banks)&nbsp;]

The <code class="language-* prism-light">POST /banks</code> command is only used if you were not able to locate your bank with the <code class="language-* prism-light">GET /banks</code> command. In this case, this command allows you to register your bank's information, including its Bank API access information and the local trustee account, with the Xand Network, and then retrieve the <code class="language-* prism-light">id</code> you receive in response as to use as the <code class="language-* prism-light">bankId</code> in subsequent steps.

This should usually not be required; in most cases, unless you are also in charge of deploying and configuring the Xand Network, your bank should already be registered, and you should have been able to look up its <code class="language-* prism-light">id</code> from the <code class="language-* prism-light">GET /banks</code> command.

### Example Request

This API call has a required request body.

The following registers a bank with ABA routing number #`"<Routing Number>"`, where the Trustee's account on the system is #`"<Routing Number>"`. It registers the bank with the arbitrary name `"<Bank Name>"`. You must indicate the correct bank-specific API adapter in order to register a bank; in this case, this bank is registered as a(n) `"<Bank Adapter Name>"` type, with appropriate access information. See also: [adapter](#adapter) specification. 

```json
{
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Bank Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API URL>",
      "secretUserName": "<Your Username Secret Key>",
      "secretPassword": "<Your Password Secret Key>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}
```

### Request Fields

#### name
[required, string]

The human-readable name of the bank. This is arbitrarily selected.

#### routingNumber
[required, string]

The ABA routing number for the bank.

#### trustAccount
[required, string]

The deposit bank account number for the Trustee at the bank.

#### adapter
[JSON object]

Defines the adapter used to communicate with a given bank's API. 

This will be one of the following: 

**`"<Bank Adapter Name">`**
 `"<Bank Adapter Name>"` JSON object containing <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretApiKeyId</code>, and <code class="language-* prism-light">secretApiKeyValue</code> (for a(n) `"<Bank Name>"` bank)

**`"<Bank Adapter Name>"`**
`"<Bank Adapter Name>"` JSON object containing <code class="language-* prism-light">url</code>, <code class="language-* prism-light">secretUserName</code>, <code class="language-* prism-light">secretPassword</code>, <code class="language-* prism-light">secretClientAppIdent</code>, and <code class="language-* prism-light">secretOrganizationId</code> (for a(n) `"<Bank Name>"` bank)

### Example Response

```json
{
  "id": <Bank Id>,
  "name": "<Bank Name>",
  "routingNumber": "<Routing Number>",
  "trustAccount": "<Trust Account Number>",
  "adapter": {
    "<Bank Adapter Name>": {
      "url": "<Bank API URL>",
      "secretUserName": "<Your Username>",
      "secretPassword": "<Your Password>",
      "secretClientAppIdent": "<Your Client Authentication>",
      "secretOrganizationId": "<Your Organization Id>"
    }
  }
}

```

#### Response Fields

All fields should be exactly as sent in with the request, with the addition of the <code class="language-* prism-light">id</code> identifier.

#### id
[integer]

The identifier of the bank.

## POST /accounts
#### Creates your Member account.

[ [API Documentation](/api/accounts#post-accounts)&nbsp;]

After you have retrieved your <code class="language-* prism-light">bankId</code>, either through the <code class="language-* prism-light">GET /banks</code> command, or by registering a bank account with the <code class="language-* prism-light">POST /banks</code> command, you can then use it to create your own account in the Xand Network. This is done by entering basic information about your bank account into the system. The result will be a Member account in the Xand network, connected to your deposit account at your bank. 

After you have entered your banking data with the <code class="language-* prism-light">POST /accounts</code> command, you will receive a response including not just your new Member account id, but also the routing information for the bank. This ABA number should be double-checked to ensure that you created your account correctly. (If the ABA number is not correct, perhaps the <code class="language-* prism-light">bankId</code> information that you entered was incorrect, and you will have to create a new Member account with the correct <code class="language-* prism-light">bankId</code>.)

> **Note**: It is helpful to think of a Member account id as a unified bank account entry in the Xand network. A member can have multiple Member accounts, one for each deposit bank account, at all supported banks. 

### Example Request

This registers the bank account <code class="language-* prism-light">1234567890</code> with the bank whose identifier is <code class="language-* prism-light">1</code> (<code class="language-* prism-light">test-bank-one</code> in the example <code class="language-* prism-light">GET /banks</code>, above) and names it <code class="language-* prism-light">Discretionary Funds</code>.

```json
{
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "accountNumber": "1234567890"
}
```
#### bankId
[required, integer]

The identifier for the Member's deposit bank. This was retrieved as the `id` from `GET /banks` (if already registered) or the `id` from `POST /banks` (if registered from that command).

#### shortName
[required, string]

A short, human-readable name for the account. Arbitrarily selected by the Member.

#### accountNumber
[required, string]

The number of the Member's deposit account at the bank.

### Example Response

```json
{
  "id": 3,
  "bankId": 1,
  "shortName": "Discretionary Funds",
  "bankName": "test-bank-one",
  "maskedAccountNumber": "XXXXXXXX7890",
  "routingNumber": "990000000"
}
```

### Response Fields

#### id
[integer]

The identifier for the Member account. This is generated by the Member API system.

#### bankId
[integer]

The identifier of the bank. This mirrors the <code class="language-* prism-light">bankId</code> that was input.

#### shortName
[string]

The short, human-readable name of the account. This mirrord the <code class="language-* prism-light">shortName</code> that was input.

#### bankName
[string]

The human-readable name of the bank. This displays the <code class="language-* prism-light">bankName</code> that is associated with the <code class="language-* prism-light">bankId</code> that was selected.

#### maskedAccountNumber
[string]

The deposit account at the bank associated with this Member. This mirrors the <code class="language-* prism-light">accountNumber</code> that was input, but partially masked to protect its security.

#### routingNumber
[string]

The ABA routing number for the bank. This displays the routing number that is associated with the <code class="language-* prism-light">bankId</code> that was selected. It should be carefully examined to ensure that it represents the correct bank for the Member's account.

## GET /member/address 
#### Retrieves your Member address for receiving claims.

After you have registered a bank deposit account in the Xand Network, you can now use your Member address on the Xand Network to fund your account, as well as send, receive, and redeem funds. Your Member address is retrieved with the <code class="language-* prism-light">GET /member/address</code> command.

### Example Request

No request body is used when accessing the <code class="language-* prism-light">GET /member/address</code> endpoint.

### Example Response

```json
{
  "address": "5DesW2g9miJMahngjaEVHap6kBMiDH33CdgndL5d75GkY7oz"
}
```

### Response Fields

#### address
[string]

The Member's address for receiving transactions on the Xand Network. Best practice is to give this address to other Members via two separate means: for example, sending it via a secure messaging system, then calling or videoing to verify the veracity of the address.
