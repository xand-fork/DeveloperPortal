module.exports = {  
	mode: 'jit',
  	content: ["./src/**/*.{html,js}"],
  theme: {
    container: {
      center: true,
      screens: {
        sm: "100%",
        md: "100%",
        lg: "1024px",
        xl: "1280px"
     }
    },
    fontFamily: {
			'sans': ['"Telegraf"', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif'],
			'body': ['"IBMPlexSans"', 'Helvetica Neue', 'Helvetica', 'Arial', 'sans-serif'],
		},
    extend: {
      colors: {
				white: '#ffffff',
				whiteish: '#F3F4F4',
				slate: {
					DEFAULT: '#0C2026',
					'light': '#6D797D',
					'lighter': '#DBDEDE',
					'lightest': '#F3F4F4',
					'dark': '#081F26',
				},
				salmon: {
					DEFAULT: '#EC4625',
					'light': '#F4907C',
					'lighter': '#FCE3DE',
					'lightest': '#FEF6F4'
				},
				gold: '#4B401F',
				miami: '#5A5AE5',
				vice: '#55F1A6',
				ltgray: '#E5EAED',
			},
      opacity: {
        '80': '0.8',
        '85': '0.85',
        '90': '0.9',
        '95': '0.95',
      },
      transitionTimingFunction: {
				'in-expo': 'cubic-bezier(0.95, 0.05, 0.795, 0.035)',
				'out-expo': 'cubic-bezier(0.19, 1, 0.22, 1)',
				'slowjerk': 'cubic-bezier(0.8, 0, 0.2, 1)',
				'in-back': 'cubic-bezier(0.600, -0.280, 0.735, 0.045)'
			},
    },
  },
  variants: {},
  plugins: [require("@tailwindcss/typography")],
};
