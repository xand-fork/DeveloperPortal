const yaml = require("js-yaml");
const { DateTime } = require("luxon");
const syntaxHighlight = require("@11ty/eleventy-plugin-syntaxhighlight");
const htmlmin = require("html-minifier");
const markdownIt = require('markdown-it')({
  html: true,
  breaks: true,
  linkify: true
});
const markdownItAttrs = require('markdown-it-attrs');
const markdownLib = markdownIt.use(markdownItAttrs)
//SVGs
// const { EleventyRenderPlugin } = require("@11ty/eleventy");
// const svgContents = require("eleventy-plugin-svg-contents");

module.exports = function (eleventyConfig) {
  // Disable automatic use of your .gitignore
  eleventyConfig.setUseGitIgnore(false);

  // Merge data instead of overriding
  eleventyConfig.setDataDeepMerge(true);

  // human readable date
  eleventyConfig.addFilter("readableDate", (dateObj) => {
    return DateTime.fromJSDate(dateObj, { zone: "utc" }).toFormat(
      "dd LLL yyyy"
    );
  });

  eleventyConfig.addFilter("replace", function(value, src, rep) {
    var reg = new RegExp(src, 'gi');
    return value.replace(reg, rep);
  });

  const inspect = require("util").inspect;

  
  eleventyConfig.addFilter("debug", (content) => `<pre>${inspect(content)}</pre>`);

  // for trimming off /s and whatnot from the template
  eleventyConfig.addFilter("trim_end", function(value, hair) {
    if(value.charAt( value.length-1 ) == hair) {
      value = value.slice(0, -1);
    }
    return value;
    // return value.replace(/hair+$/, "");
  });

  //SVGs
  // eleventyConfig.addPlugin(svgContents);
  // eleventyConfig.addPlugin(EleventyRenderPlugin);
  

  // Syntax Highlighting for Code blocks
  eleventyConfig.addPlugin(syntaxHighlight);

  // To Support .yaml Extension in _data
  // You may remove this if you can use JSON
  eleventyConfig.addDataExtension("yaml", (contents) =>
    yaml.safeLoad(contents)
  );

  eleventyConfig.setDataDeepMerge(true);
  // Create collection from _data/customData.js
  // eleventyConfig.addCollection("apiCollection", (collection) => {
  //   const allItems = collection.getAll()[0].data.api;

  //   // Filter or use another method to select the items you want
  //   // for the collection
  //   return allItems.filter((item) => {
  //     // ...
  //   });
  // });

  // eleventyConfig.addCollection('articles', async collection => {
  //   const response = await getEndpoint({ url: host + '/api/articles' })
  //   return response.data
  // })

  // Copy Static Files to /_Site
  eleventyConfig.addPassthroughCopy({
    "./src/admin/config.yml": "./admin/config.yml",
    "./node_modules/alpinejs/dist/cdn.min.js": "./static/js/alpine.js",
    // "./node_modules/prismjs/themes/prism-tomorrow.css": "./static/css/prism-tomorrow.css",
  });

  // Copy Image Folder to /_site
  eleventyConfig.addPassthroughCopy("./src/static/img");
  
  // Copy fonts folder to /_site
  eleventyConfig.addPassthroughCopy("./src/static/fonts");

  // Copy favicon to route of /_site
  eleventyConfig.addPassthroughCopy("./src/favicon.ico");

  // Minify HTML
  eleventyConfig.addTransform("htmlmin", function (content, outputPath) {
    // Eleventy 1.0+: use this.inputPath and this.outputPath instead
    if (outputPath.endsWith(".html")) {
      let minified = htmlmin.minify(content, {
        useShortDoctype: true,
        removeComments: true,
        collapseWhitespace: true
      });
      return minified;
    }

    return content;
  });

  //add markdown attributes
  eleventyConfig.setLibrary('md', markdownLib);
  
  eleventyConfig.addFilter('markdown', value => {
    return `${markdownIt.render(value)}`
  });

  // Let Eleventy transform HTML files as nunjucks
  // So that we can use .html instead of .njk
  return {
    dir: {
      input: "src",
    },
    htmlTemplateEngine: "njk",
  };
};
