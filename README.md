# Transparent Developer Experience Portal

## License

MIT OR Apache-2.0

## Content Source of Truth

The source of truth for content that should be deployed on the site can be found in this repository: https://gitlab.com/TransparentIncDevelopment/docs/dxp

If there is ever a question on what should be displayed on the DXP site as content, the above repository should be used.  There are some differences in formatting for the content used on this website, but never actual content.

> Note: There are currently some edits to the content to anonymize banking partners.  **This is temporary**.  To undo this anonymization, revert [this commit](https://gitlab.com/TransparentIncDevelopment/docs/dxp/-/commit/29895ff7d46cf4f06c87e3ceb4e868902591c56a) and use the reverted content.

## Xandbox Request-Access Form Control

Submissions for the request-access form on the Xandbox page are collected via a Mailchimp action.  Please reach out to the maintainer of the Mailchimp account for any changes.

## Technologies used:

- [Netlify CMS](https://www.netlifycms.org/)
- [Eleventy](https://www.11ty.dev/)
- [Alpine.js](https://github.com/alpinejs/alpine)
- [Tailwind CSS](https://tailwindcss.com/)

## Usage Instructions

### 1\. Clone this Repository

```
git clone git@gitlab.com:TransparentIncDevelopment/product/Website/DeveloperPortal.git
```

### 2\. Navigate to the directory

```
cd DeveloperPortal
```

### 3\. Install dependencies

```
npm install
```

### 4\. Build the project to generate the first CSS

This step is only required the very first time.

```
npm run build
```

### 5\. Run Eleventy

```
npm run start
```

