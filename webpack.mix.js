let mix = require('laravel-mix');
const tailwindcss = require('tailwindcss'); 
require('laravel-mix-tailwind');
require('laravel-mix-polyfill');
require('laravel-mix-purgecss');



mix
    .js(['src/static/js/prism.js','src/static/js/app.js'], 'static/js/app.js')
	.tailwind()
    .sass('src/static/scss/app.scss', 'static/css/app.css')
    .sass('src/static/scss/preview.scss', 'static/css/preview.css')
    .sourceMaps()
    .setPublicPath('_site')
    .version();
